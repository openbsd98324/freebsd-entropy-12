# freebsd-entropy-12



## Getting started

FreeBSD for notebooks, acer and intel 



![](media/freebsd-notebook-v2.png)




## Live memstick

https://gitlab.com/openbsd98324/freebsd-entropy-12/-/raw/main/qemu-image/FreeBSD-memstick-da0-12.0-RELEASE-r328126-Generic-i386-x86_64-live-vdisk-installed-running-ready-with-bsdinstall-console-v3.img.gz


![](media/memstick-v3.png)




## Further installations

Create /usr/freebsd-dist/   with the packages 

 /usr/freebsd-dist  for BSDINSTALL_CHROOT



````
 bsdinstall
````


